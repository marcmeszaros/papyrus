/**
 * Copyright 2011 Marc Meszaros
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.marcmeszaros.papyrus.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class TNManager {

    public static final String COVERS_DIR = "covers";

	private static final String TAG = "TNManager";

	/**
	 * Saves the {@link android.graphics.Bitmap} thumbnail of a book to the SD card.
	 *
     * @param context the Android context
	 * @param bitmap the {@link android.graphics.Bitmap} to save
	 * @param isbn the ISBN number of the book
	 * @return {@code true} on success or {@code false} on failure
	 */
	public static boolean saveThumbnail(Context context, Bitmap bitmap, String isbn) {
		// make sure we have access to the SD card
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			try {
                // get the cover files directory
                File coversDir = context.getExternalFilesDir(COVERS_DIR);

				// create the thumbnail
				File thumbnail = new File(coversDir, isbn + ".jpg");

				// if the file doesn't exist, create it and get the data
				if (!thumbnail.exists()) {
					thumbnail.createNewFile();
					FileOutputStream out = new FileOutputStream(thumbnail);

					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
				}

			} catch (IOException e) {
				Log.e(TAG, "IOException", e);
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Return a File handler to the book thumbnail.
	 *
     * @param context the Android context
	 * @param isbn the ISBN number to the book to get
	 * @return a {@link java.io.File} handle to the thumbnail image
	 */
	public static File getThumbnail(Context context, String isbn) {
		return new File(context.getExternalFilesDir(COVERS_DIR), isbn + ".jpg");
	}

	/**
	 * Delete a book thumbnail using it's ISBN number.
	 *
     * @param context the Android context
	 * @param isbn the ISBN number to delete
	 * @return {@code true} on operation success or {@code false} on failure
	 */
	public static boolean deleteThumbnail(Context context, String isbn) {
		return new File(context.getExternalFilesDir(COVERS_DIR), isbn + ".jpg").delete();
	}

}
