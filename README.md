# Overview #
Papyrus enables users to create a virtual library of all the books
they own. Users can also keep track of loans they make to other people.

Users can add books in a variety of ways. The easiest way is to use the barcode scanning
functionality to scan the ISBN barcode. Papyrus attempts to get the book
information from the Google Books service.

Multiple libraries are supported by Papyrus and so are multiple copies of books.
This feature gives the user finer grained control over how they manage their
books.

# Getting Started #
1. Make sure to uninstall/reinstall the Android SDK (SDK folder/file layouts have changed and setup is based on SDK r21+)
2. Install the Android 2.3.3 (API 10) and Android Support Package from the SDK tools
3. Proceed with any of the setup instructions below
4. Make sure your system has Maven installed.

## Eclipse ##
1. Make sure the [ADT Plugin](http://developer.android.com/sdk/installing/installing-adt.html) is installed.
2. Start up Eclipse and go to `File->Import...`
3. Select `Maven->Existing Maven Projects`
4. Browse to the root folder of the android project and select it.
5. Select the sub-projects to include (choose all of them if you aren't sure) and click `Finish`.

## Maven/CLI ##
1. Done. Run some maven commands. :)

# Basic Maven Commands #
Below are some basic Maven commands to do some common tasks. (Assumes you are in the project root directory.)

    # debug build, with unit tests
    mvn clean install

    # debug build, no unit tests (faster)
    mvn -pl app clean compile package android:deploy

    # release build, no unit tests (keystore and release.properties file must exist)
    mvn -pl app -P release clean install
